<!DOCTYPE html>
<html lang="en">

<head>
<title>Classificador de Processos</title>
<!-- HTML5 Shim and Respond.js IE9 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<!-- Meta -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="description"
	content="Gradient Able Bootstrap admin template made using Bootstrap 4. The starter version of Gradient Able is completely free for personal project." />
<meta name="keywords"
	content="flat ui, admin , Flat ui, Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
<meta name="author" content="codedthemes">
<!-- Favicon icon -->
<link rel="icon" href="{{ asset('assets/images/favicon.ico')}}"
	type="image/x-icon">
<!-- Google font-->
<link
	href="{{ asset('https://fonts.googleapis.com/css?family=Poppins:300,400,500,600')}}"
	rel="stylesheet">
<!-- Required Fremwork -->
<link rel="stylesheet" type="text/css"
	href="{{ asset('assets/css/bootstrap/css/bootstrap.min.css')}}">
<!-- themify-icons line icon -->
<link rel="stylesheet" type="text/css"
	href="{{ asset('assets/icon/themify-icons/themify-icons.css')}}">
<link rel="stylesheet" type="text/css"
	href="{{ asset('assets/icon/font-awesome/css/font-awesome.min.css')}}">

<!-- ico font -->
<link rel="stylesheet" type="text/css"
	href="{{ asset('assets/icon/icofont/css/icofont.css')}}">
<!-- Style.css -->
<link rel="stylesheet" type="text/css"
	href="{{ asset('assets/css/style.css')}}">
<link rel="stylesheet" type="text/css"
	href="{{ asset('assets/css/jquery.mCustomScrollbar.css')}}">

	<meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body>
	<!-- Pre-loader start -->
	<div class="theme-loader">
		<div class="loader-track">
			<div class="loader-bar"></div>
		</div>
	</div>
	<!-- Pre-loader end -->
	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">

			<nav class="navbar header-navbar pcoded-header">
				<div class="navbar-container container-fluid">
					<span></span>
				</div>
			</nav>

			<div class="pcoded-main-container">
				<div class="pcoded-wrapper">
					<div class="pcoded-content">
						<div class="pcoded-inner-content">

							<div class="main-body">
								<div class="page-wrapper">
									@if ($errors->any())
                                        <div class="alert alert-danger mb-2" role="alert">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li><i class="icon-info-circle"></i> {{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    @if(session()->get('success'))
                                    <div class="alert alert-success mb-2">
                                      {{ session()->get('success') }}
                                    </div><br />
                                    @endif
									<!-- Page-header start -->
									<div class="page-header card">
										<div class="card-block">
											<h5 class="m-b-10">Classificador de Processos</h5>
											<p class="text-muted m-b-10">Nessa tela, você classificará os
												processos de acordo com o assunto que julgar correto</p>
										</div>
									</div>
									<!-- Page-header end -->


									<div class="page-body">
										<div class="row">
											<div class="col-sm-12">
												<div class="card">
													<form action="{{ route('processo.classificar') }}" id="form-classificador" method="post">
														@csrf
														<div class="card-header">
															<h5>Processos</h5>
															<div class="form-group row">
																<label class="col-sm-1 col-form-label">Classificador</label>
																<div class="col-sm-6">
																	<input type="text" required="required" name="classificador"
																		class="form-control" placeholder="Classificador">
																</div>
															</div>
															<div class="card-header-right">
																<ul class="list-unstyled card-option">
																	<li><i class="fa fa-chevron-left"></i></li>
																	<li><i class="fa fa-window-maximize full-card"></i></li>
																	<li><i class="fa fa-minus minimize-card"></i></li>
																	<li><i class="fa fa-refresh reload-card"></i></li>
																	<li><i class="fa fa-times close-card"></i></li>
																</ul>
															</div>
														</div>
														<div class="card-block table-border-style">
															<div class="table-responsive">
																<table class="table table-hover table-border-style">
																	<thead>
																		<tr>
																			<th>#</th>
																			<th>Número do Processo</th>
																			<th>Assunto Principal</th>
																			<th></th>
																		</tr>
																	</thead>
																	<tbody>
																		<?php

$i = 1?>
																		@foreach ($processos as $processo)
																		<tr>
																			<th scope="row">{{$i}}</th>
																			<td><a type="button" class="inicial" id="{{$processo->id}}" data-process="{{$processo->num_processo}}" data-toggle="modal" data-target="#exampleModal" tooltip title="Mostrar" href="#"><i class="ti-write"></i><span
																					class="col-sm-12 col-md-6 col-lg-4 col-xl-3 outer-ellipsis">{{$processo->num_processo}}</span></a>
																				<input type="hidden" name="processo_id[]"
																				value="{{$processo->id}}" /></td>
																			<td>
																				<div class="col-sm-9">
																					<select name="assunto_principal_id[]" required="required" class="form-control">
																						<option value="" >Assunto Principal</option>
																						@foreach ($assuntosJudiciais as $assunto)
																							<option value="{{$assunto->id}}">{{$assunto->des_assunto}} - {{$assunto->cod_assunto}}</option>
																						@endforeach
																					</select>
																				</div>
																			</td>
																			<td></td>
																		</tr>
																		<?php

$i ++?>
																		@endforeach
																	</tbody>
																</table>
															</div>
														</div>
														<div class="card-footer">
															<div class="col-sm-12 text-center">
																<input type="submit" class="btn btn-primary waves-effect waves-light" title="Salvar Classificação" value="Salvar" />
															</div>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div id="styleSelector"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- Início Modal -->

<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="max-width:85%">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Petição Inicial</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">

      </div>
    </div>
  </div>
</div>

<!-- Fim Modal -->

	<!-- Warning Section Starts -->
	<!-- Older IE warning message -->
	<!--[if lt IE 9]>
<div class="ie-warning">
    <h1>Warning!!</h1>
    <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers
        to access this website.</p>
    <div class="iew-container">
        <ul class="iew-download">
            <li>
                <a href="http://www.google.com/chrome/">
                    <img src="assets/images/browser/chrome.png" alt="Chrome">
                    <div>Chrome</div>
                </a>
            </li>
            <li>
                <a href="https://www.mozilla.org/en-US/firefox/new/">
                    <img src="assets/images/browser/firefox.png" alt="Firefox">
                    <div>Firefox</div>
                </a>
            </li>
            <li>
                <a href="http://www.opera.com">
                    <img src="assets/images/browser/opera.png" alt="Opera">
                    <div>Opera</div>
                </a>
            </li>
            <li>
                <a href="https://www.apple.com/safari/">
                    <img src="assets/images/browser/safari.png" alt="Safari">
                    <div>Safari</div>
                </a>
            </li>
            <li>
                <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                    <img src="assets/images/browser/ie.png" alt="">
                    <div>IE (9 & above)</div>
                </a>
            </li>
        </ul>
    </div>
    <p>Sorry for the inconvenience!</p>
</div>
<![endif]-->
	<!-- Warning Section Ends -->
	<!-- Required Jquery -->
	<script type="text/javascript"
		src="{{ asset('assets/js/jquery/jquery.min.js') }}"></script>
	<script type="text/javascript"
		src="{{ asset('assets/js/jquery-ui/jquery-ui.min.js') }}"></script>
	<script type="text/javascript"
		src="{{ asset('assets/js/popper.js/popper.min.js') }}"></script>
	<script type="text/javascript"
		src="{{ asset('assets/js/bootstrap/js/bootstrap.min.js') }}"></script>
	<!-- jquery slimscroll js -->
	<script type="text/javascript"
		src="{{ asset('assets/js/jquery-slimscroll/jquery.slimscroll.js') }}"></script>
	<!-- modernizr js -->
	<script type="text/javascript"
		src="{{ asset('assets/js/modernizr/modernizr.js') }}"></script>
	<script type="text/javascript"
		src="{{ asset('assets/js/modernizr/css-scrollbars.js') }}"></script>

	<!-- Custom js -->
	<script type="text/javascript" src="{{ asset('assets/js/script.js') }}"></script>
	<script src="{{ asset('assets/js/pcoded.min.js') }}"></script>
	<script src="{{ asset('assets/js/vartical-demo.js') }}"></script>
	<script
		src="{{ asset('assets/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>


<script type="text/javascript">
	$(document).ready(function() {

		var pdf = '<object type="application/pdf" id="inicial_pdf" data="" width="100%" height="80%" style="height: 90vh; width: 100%">No Support</object>';


	    $(".inicial").click(function() {
	    	$("object").remove();
			$("h3").remove();

	    	$.ajaxSetup({
	            headers: {
	                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
	            }
	        });
	      var _id = $(this).attr("id");
	      var processo = $(this).attr("data-process");
	      $.ajax({
	        url: '{{ route('processo.inicial') }}',
	        type: 'GET',
	        data:'id='+processo,
	        xhrFields: {
	            responseType: 'blob'
	        },
	        success:function(data) {
             	$('.modal-body').append(pdf);
		        console.log(data);
	        	var a = document.createElement('a');
	            var url = window.URL.createObjectURL(data);
	        	$('#inicial_pdf').attr('data', url);

            },
            error: function(xhr, status, error) {
                 console.log(error)
                 $("object").remove();
                 $('.modal-body').append('<h3>Não foi possível visualizar o processo</h3>');
            }
        });
      })
    });

</script>
</body>

</html>
