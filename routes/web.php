<?php
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

/*
 * |--------------------------------------------------------------------------
 * | Web Routes
 * |--------------------------------------------------------------------------
 * |
 * | Here is where you can register web routes for your application. These
 * | routes are loaded by the RouteServiceProvider within a group which
 * | contains the "web" middleware group. Now create something great!
 * |
 */

Route::get('/', function () {
    return view('processo.listar');
});

Route::get('/processos', 'ProcessoController@pesquisar')->name('processos');

Route::post('/classificar', 'ProcessoController@classificar')->name('processo.classificar');

Route::any('/inicial', 'ProcessoController@visualizar')->name('processo.inicial');

Route::get("/inicial/{file}", function ($file = "") {

    $headers = array(
        'Content-Type: application/pdf',
        'Content-Disposition' => 'inline'
    );
    return Storage::disk('public')->download('processo/' . $file . '.pdf', $file . '.pdf', $headers);
    // return response()->download(storage_path("app/public/".$file));
});