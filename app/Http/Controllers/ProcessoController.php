<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Entities\Judicial\AssuntoJudicial;
use App\Http\Entities\Admin\Processo;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
use App\Http\Entities\Admin\ProcessoClassificado;

class ProcessoController extends Controller
{

    const ASSUNTOSJUDICIAIS = [
        7698,
        9582,
        9597,
        4970,
        9996,
        6226,
        6104,
        7767,
        10437,
        9587,
        7626,
        11000,
        10671,
        9606,
        10677,
        11806,
        10671,
        7704,
        7768
    ];

    public function pesquisar()
    {
        $assuntosJudiais = AssuntoJudicial::wherein('id_assunto_judicial', self::ASSUNTOSJUDICIAIS)->orderBy('des_assunto')->get();
        $processos = Processo::all();

        return view('processo.listar')->with('assuntosJudiciais', $assuntosJudiais)->with('processos', $processos);
    }

    public function visualizar(Request $input)
    {
        $numeroProcesso = $input->get('id');
        Log::alert($numeroProcesso);
        $headers = array(
            'Content-Type: application/pdf',
            'Content-Disposition' => 'inline'
        );
        return Storage::disk('public')->download('processo/' . $numeroProcesso . '.pdf', $numeroProcesso . '.pdf', $headers);
    }

    public function classificar(Request $input)
    {
        $classificador = $input->get('classificador');
        $processos = $input->get('processo_id');
        $assuntos = $input->get('assunto_principal_id');

        if (count($processos) == count($assuntos)) {
            for ($i = 0; $i < count($processos); $i ++) {
                $processo = Processo::find($processos[$i]);
                ProcessoClassificado::create([
                    'processo_id' => $processo->id,
                    'numemro_processo' => $processo->num_processo,
                    'assunto_id' => $assuntos[$i],
                    'classificador' => $classificador
                ]);
            }
        } else {
            throw new \Exception('Um dos processos listados não foi classificado');
        }

        $assuntosJudiais = AssuntoJudicial::wherein('cod_mni_assunto_judicial', self::ASSUNTOSJUDICIAIS)->orderBy('des_assunto')->get();
        $processos = Processo::all();

        return redirect('processos')->with('success', 'Processos classificado com sucesso, obrigado');
    }

    public function consultar(Request $input)
    {
        return view('judicial.processo.pesquisar');
    }

    public function listar(Request $input)
    {
        $processos = $this->service->listarProcessos($input);

        return view('judicial.processo.pesquisar')->with('processos', $processos);
    }

    public function distribuir(Request $input)
    {}
}
