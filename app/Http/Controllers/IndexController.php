<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Helpers\ModuloStyle;
use App\Http\Services\Base\UsuarioService;

/**
 * Class IndexController
 *
 * @package App\Http\Controllers\Index
 */
class IndexController extends Controller
{

    protected $service;

    /**
     * IndexController constructor.
     *
     * @param UsuarioService $usuarioService
     */
    public function __construct(UsuarioService $usuarioService)
    {
        $this->service = $usuarioService;
    }

    public function index()
    {
        return view(ModuloStyle::getIndex() . '.index');
    }
}
