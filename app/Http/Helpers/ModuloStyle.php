<?php
namespace App\Http\Helpers;

use App\Http\Enum\Modulo;
use App\Http\Services\Base\PermissaoService;
use Illuminate\Support\Facades\Route;
use App\Http\Enum\Base\ModuloEnum;

class ModuloStyle
{

    public static function getNameSpace()
    {
        $rota_corrente = Route::getFacadeRoot()->current()->action['as'];
        $permissao = app(PermissaoService::class)->findBy([
            [
                'rota',
                $rota_corrente
            ]
        ])->first();

        if (! $permissao) {
            $permissao = app(PermissaoService::class)->findBy([
                [
                    'dependencia',
                    'like',
                    "%" . $rota_corrente . "%"
                ]
            ])->first();
            if ($permissao) {
                $modulo = $permissao->modulo;
            }
        } else {
            $modulo = $permissao->modulo;
        }

        if (! isset($modulo)) {
            dd("Modulo não encontrado");
        }

        return $modulo;
    }

    public static function getModuloAtivo()
    {
        $modulo = self::getNameSpace();
        $modulo_corrente = null;
        foreach (ModuloEnum::MODULOS as $key => $row) {
            if ($key == $modulo) {
                $modulo_corrente = $key;
                break;
            }
        }
        if (! isset($modulo_corrente)) {
            dd("Modulo Então encontrado");
        }

        return $modulo_corrente;
    }

    public static function getModuloNome()
    {
        $modulo = self::getModuloAtivo();
        return ModuloEnum::MODULOS[$modulo]['label'];
    }

    public static function getIndex()
    {
        $modulo = self::getModuloAtivo();
        return strtolower(ModuloEnum::MODULOS[$modulo]['namespace']);
    }

    public static function getTheme()
    {
        $modulo = self::getModuloAtivo();
        return ModuloEnum::MODULOS[$modulo]['skin'];
    }

    public static function getIndexPorModulo($modulo)
    {
        return [
            strtolower(ModuloEnum::MODULOS[$modulo]['namespace']),
            ModuloEnum::MODULOS[$modulo]['label']
        ];
    }

    public static function getListaDeModulos()
    {
        foreach (ModuloEnum::MODULOS as $modulo => $atributos) {
            $lista[$modulo] = $atributos['label'];
        }
        return collect($lista);
    }

    public static function getListaDeModulosAbertos()
    {
        foreach (ModuloEnum::MODULOS as $modulo => $atributos) {
            if ($atributos['vision'] == "aberto") {
                $lista[$modulo] = $atributos['label'];
            }
        }
        return collect($lista);
    }
}
