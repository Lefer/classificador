<?php
namespace App\Http\Helpers;

/**
 *
 * @author Leandro
 *
 */
class AplicacaoHelper
{

    public static function somenteNumeros($value)
    {
        return preg_replace('/[^0-9]/', '', $value);
        ;
    }

    public static function getTelefoneLabel($value)
    {
        return preg_replace('/(\d{2})(\d{4})(\d{4})/', '($1) $2-$3', $value);
    }

    public static function getCelularLabel($value)
    {
        return preg_replace('/(\d{2})(\d{5})(\d{4})/', '($1) $2-$3', $value);
    }

    public static function getTelefoneCelular($value)
    {
        if (strlen($value) > 10) {
            return preg_replace('/(\d{2})(\d{5})(\d{5})/', '($1) $2-$3', $value);
        } else {
            return preg_replace('/(\d{2})(\d{5})(\d{4})/', '($1) $2-$3', $value);
        }
    }

    public static function removerPontuacaoCep($data)
    {
        $data = str_replace(".", '', $data);
        $data = str_replace("-", '', $data);

        return $data;
    }

    public static function getCepLabel($value)
    {
        return preg_replace('/(\d{2})(\d{3})(\d{3})/', '$1.$2-$3', $value);
    }

    public static function getCPFCNPJLabel($value)
    {
        if (strlen($value) == 11) {
            return preg_replace('/(\d{3})(\d{3})(\d{3})(\d{2})/', '$1.$2.$3-$4', $value);
        } elseif (strlen($value) == 14) {
            return preg_replace('/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/', '$1.$2.$3/$4-$5', $value);
        }
        return $value;
    }

    /*
     * Datatable Componente
     */

    /*
     * Buscar Valor do objeto e verificar se vai navegar nos relacionamento
     */
    public static function getLabel($object, $valor)
    {
        $prop = explode('.', $object);
        $label = null;
        if (is_object($valor)) {
            foreach ($prop as $v) {
                if (! $label) {
                    $label = $valor->$v;
                    if (isset($label[0])) {
                        if (is_object($label[0])) {
                            $label = $label[0];
                        }
                    }
                } else {
                    $label = $label->$v;
                }
            }
        } elseif (is_array($valor)) {
            foreach ($prop as $v) {
                if (! $label) {
                    $label = $valor[$v];
                    if (isset($label[0])) {
                        if (is_object($label[0])) {
                            $label = $label[0];
                        }
                    }
                } else {
                    $label = $label[$v];
                }
            }
        }

        return $label;
    }

    public static function getAlias($row, $atributtes, $current_object)
    {
        $label = self::getLabel($atributtes, $current_object);
        if (isset($row['alias'])) {
            $temp_label = null;
            foreach ($row['alias'] as $alias => $valor_original) {
                if ($valor_original == $label) {
                    $temp_label = $alias;
                }
            }
            if ($temp_label) {
                $label = $temp_label;
            }
        }
        return $label;
    }

    public static function removerCheckAll($checklist)
    {
        foreach ($checklist as $key => $check) {
            if ($check == 'on') {
                unset($checklist[$key]);
            }
        }
        return $checklist;
    }

    public static function isBool($bool)
    {
        if (! isset($bool)) {
            return false;
        }
        if ($bool == 0) {
            return false;
        }
        return true;
    }

    public static function completaComZeros($string, $quantidade, $direcao = 'STR_PAD_LEFT')
    {
        if ($direcao == 'STR_PAD_RIGHT') {
            return str_pad($string, $quantidade, "0", STR_PAD_RIGHT);
        }

        return str_pad($string, $quantidade, "0", STR_PAD_LEFT);
    }
}
