<?php
namespace App\Http\Helpers;

use App\Http\Entities\Base\Usuario;
use App\Http\Entities\Base\Divisao;
use Route;
use Illuminate\Support\Facades\Log;
use App\Exceptions\Base\EpgeException;

class SessionHelper
{

    /**
     *
     * @param Usuario $usuario
     * @param Instituicao $divisao
     */
    public static function setSessionUsuario(Usuario $usuario, Divisao $divisao, $divisoes)
    {
        session([
            'usuario' => [
                'id' => $usuario->id,
                'nome' => $usuario->nome,
                'divisao' => $divisao,
                'divisoes' => $divisoes,
                'modulo' => $usuario->modulo_padrao
            ]
        ]);
    }

    /**
     *
     * @param
     *            $modulo
     */
    public static function setModuloUsuario($modulo)
    {
        $modulo = strtoupper($modulo);

        if (! isset($modulo)) {
            Log::error("O modulo selecionado não existe!");
            new EpgeException("O modulo selecionado não existe!");
            return false;
        }

        session([
            'usuario' => [
                'id' => self::getIdUsuario(),
                'nome' => self::getNomeUsuario(),
                'divisao' => self::getDivisao(),
                'divisoes' => self::getDivisoes(),
                'modulo' => $modulo
            ]
        ]);
        list ($modulo, $label) = ModuloStyle::getIndexPorModulo($modulo);
        return [
            $modulo,
            $label
        ];
    }

    public static function setDivisao(Divisao $divisao)
    {
        session([
            'divisao' => $divisao
        ]);
    }

    /**
     *
     * @return sessão do usuário
     */
    public static function getSessionUsuario(): Usuario
    {
        return session()->get('usuario');
    }

    /**
     *
     * @return idUsuario
     */
    public static function getIdUsuario(): int
    {
        return session()->get('usuario')['id'];
    }

    /**
     *
     * @return nome do usuario
     */
    public static function getNomeUsuario(): string
    {
        return session()->get('usuario')['nome'];
    }

    /**
     *
     * @return Divisao divisao
     */
    public static function getDivisao(): Divisao
    {
        if (isset(session()->get('usuario')['divisao'])) {
            return session()->get('usuario')['divisao'];
        }
        return session()->get('divisao');
    }

    /**
     *
     * @return int divisao->id
     */
    public static function getIdDivisao(): int
    {
        return session()->get('usuario')['divisao']['id'];
    }

    /**
     *
     * @return mixed
     */
    public static function getDivisoes()
    {
        return session()->get('usuario')['divisoes'];
    }

    /**
     *
     * @return mixed
     */
    public static function getModulo()
    {
        return ModuloStyle::getModuloAtivo();
    }

    /**
     *
     * @return apaga sessão usuario
     */
    public static function destroySessionUsuario()
    {
        return session()->forget('usuario');
    }

    /**
     *
     * @return string
     */
    public static function route()
    {
        if (Route::getFacadeRoot()->current() == null) {
            return 'gc:service';
        }
        return Route::getFacadeRoot()->current()->action['as'];
    }

    /**
     *
     * @return string
     */
    public static function isServicoGestaoCartorio()
    {
        if (Route::getFacadeRoot()->current() == null) {
            return true;
        }
        return false;
    }
}
