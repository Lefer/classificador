<?php
namespace App\Http\Entities\Base;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Http\Entities\Base\BaseEntity
 *
 * @mixin \Eloquent
 */
class BaseEntity extends Model
{

    use BaseEntityDivisaoTrait;

    public $timestamps = true;

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected function toString()
    {
        return $this->__toString();
    }
}
