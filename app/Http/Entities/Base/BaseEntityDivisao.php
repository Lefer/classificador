<?php
namespace App\Http\Entities\Base;

use App\Http\Entities\Base\Divisao;

/**
 * App\Http\Entities\Base\BaseEntityDivisao
 *
 * @mixin \Eloquent
 */
class BaseEntityDivisao extends BaseEntity
{

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function divisao()
    {
        return $this->belongsTo(Divisao::class, 'divisao_id');
    }
}
