<?php
namespace App\Http\Entities\Base;

use App\Http\Helpers\SessionHelper;
use App\Http\Enum\Base\TipoDivisaoEnum;

trait BaseEntityDivisaoTrait
{

    /**
     *
     * @param string $columnWhere
     * @param array $columns
     * @return mixed
     */
    public static function allByDivisao($columnWhere = 'divisao_id', $columns = ['*'], $param = null)
    {
        $divisaoSessao = SessionHelper::getDivisao();

        if (! isset($divisaoSessao)) {
            return null;
        }

        if (TipoDivisaoEnum::isPermissaoGlobal($divisaoSessao->tipodivisao)) {
            return parent::all();
        }

        $query = parent::where($columnWhere, '=', $divisaoSessao->id);

        // Verificar Order By
        if (isset($param['orderBy'])) {
            $type = (isset($param['type'])) ? $param['type'] : 'ASC';
            $query->orderBy($param['orderBy'], $type);
        }

        // Verifica LIMIT
        if (isset($param['limit'])) {
            $query->limit($param['limit']);
        }

        // Verifica WhereIn
        if (isset($param['whereIn'])) {
            $query->WhereIn($param['whereIn'][0], $param['whereIn'][1]);
        }

        return $query->get();
    }
}
