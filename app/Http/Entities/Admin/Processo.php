<?php
namespace App\Http\Entities\Admin;

use App\Http\Entities\Base\BaseEntityDivisao;
use Illuminate\Database\Eloquent\SoftDeletes;

class Processo extends BaseEntityDivisao
{

    use SoftDeletes;

    protected $table = 'processo';

    protected $fillable = [
        'id_processo',
        'seq_processo',
        'num_processo',
        'id_assunto_principal',
        'id_assunto_principal_classificado',
        'data_classificacao',
        'classificador',
        'status'

    ];
}
