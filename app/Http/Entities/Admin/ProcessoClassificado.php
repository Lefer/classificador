<?php
namespace App\Http\Entities\Admin;

use App\Http\Entities\Base\BaseEntityDivisao;

class ProcessoClassificado extends BaseEntityDivisao
{

    protected $table = 'processo_classificado';

    protected $fillable = [
        'processo_id',
        'numemro_processo',
        'assunto_id',
        'classificador'

    ];
}
