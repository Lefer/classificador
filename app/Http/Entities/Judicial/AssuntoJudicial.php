<?php
namespace App\Http\Entities\Judicial;

use App\Http\Entities\Base\BaseEntityDivisao;

class AssuntoJudicial extends BaseEntityDivisao
{

    protected $table = 'assunto_judicial';

    protected $fillable = [
        'id_assunto_judicial',
        'cod_assunto',
        'des_assunto',
        'cod_mni_assunto_judicial',
        'tipo_assunto'
    ];
}