<?php
namespace App\Http\Services\Base;

use App\Http\Services\Base\Interfaces\ServiceBaseInterface;
use App\Http\Helpers\SessionHelper;
use App\Http\Repositories\Base\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Log;
use Exception;
use App\Http\Entities\Base\TipoDivisao;
use App\Exceptions\Base\EpgeException;

/**
 * Class BaseService
 *
 * @package App\Http\Services\Base
 */
abstract class BaseService extends Application implements ServiceBaseInterface
{

    protected $repository;

    /**
     * BaseService constructor.
     *
     * @internal param $repository
     * @param BaseRepository $repository
     */
    public function __construct(BaseRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @param array $data
     * @return Model
     * @throws Exception
     */
    public function create(array $data)
    {
        try {
            return $this->repository->create($data);
        } catch (Exception $e) {
            throw $e;
        }
        return null;
    }

    /**
     *
     * @param array $data
     * @return Model
     * @throws Exception
     */
    public function createByDivisao(array $data)
    {
        if (! isset($data['divisao_id'])) {
            $data['divisao_id'] = SessionHelper::getIdDivisao();
        }
        try {
            return $this->repository->create($data);
        } catch (Exception $e) {
            throw $e;
        }
        return null;
    }

    /**
     *
     * @param Model $entity
     * @return Model
     * @throws Exception
     */
    public function insert($entity)
    {
        try {
            return $this->repository->insert($entity);
        } catch (\Exception $e) {
            throw $e;
        }
        return null;
    }

    /**
     *
     * @param Model $entity
     * @return Model|null
     * @throws Exception
     */
    public function update($entity)
    {
        try {
            return $this->repository->update($entity);
        } catch (Exception $e) {
            throw $e;
        }
        return null;
    }

    /**
     * Atualiza uma array de dados de acordo com o id do Model informado
     *
     * @param
     *            $id
     * @param array $dados
     * @throws Exception
     * @return Model
     */
    public function merge($id, array $dados)
    {
        try {
            $model = $this->findById($id);
            $model->fill($dados);
            return $this->update($model);
        } catch (Exception $e) {
            throw $e;
        }
        return null;
    }

    /**
     *
     * @param Model $entity
     * @return null
     * @throws Exception
     */
    public function delete($entity)
    {
        try {
            return $this->repository->delete($entity);
        } catch (Exception $e) {
            throw $e;
        }
        return null;
    }

    /**
     *
     * @return null
     * @throws Exception
     */
    public function all()
    {
        try {
            return $this->repository->findAll();
        } catch (Exception $e) {
            throw $e;
        }
        return null;
    }

    /**
     *
     * @param unknown $id
     * @throws EpgeException
     * @throws Exception
     * @return mixed
     */
    public function find($id)
    {
        try {
            $entity = $this->repository->findById($id);
            // verifica se a entidade buscada tem instituicao_id
            if (isset($entity->divisao_id)) {
                // verifica se a instiuicao da entidade é a mesma do solicitante.
                if ($entity->divisao_id == SessionHelper::getIdDivisao() || TipoDivisao::isPermissaoGlobal(SessionHelper::getDivisao()->tipodivisao)) {
                    return $entity;
                } else {
                    Log::error('O usuário ' . SessionHelper::getIdUsuario() . ' tentou acessar o recurso ' . $entity->id);
                    throw new EpgeException('Esse recurso não pertence a essa instituição');
                }
            }

            return $entity;
        } catch (Exception $exception) {
            Log::error($exception->getMessage() . $exception->getTraceAsString());
            throw $exception;
        }
    }

    /**
     *
     * @param
     *            $column
     * @return Long|null
     * @throws Exception
     */
    public function max($column)
    {
        try {
            return $this->repository->max($column);
        } catch (Exception $e) {
            throw $e;
        }
        return null;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \App\Http\Services\Base\ServiceBaseInterface::findById()
     */
    public function findById($id)
    {
        try {
            $entity = $this->repository->findById($id);
            // verifica se a entidade buscada tem instituicao_id
            if (isset($entity->instituicao_id)) {
                // verifica se a instiuicao da entidade é a mesma do solicitante.
                if ($entity->instituicao_id == SessionHelper::getIdDivisao() || TipoDivisao::isPermissaoGlobal(SessionHelper::getDivisao()->tipodivisao)) {
                    return $entity;
                } else {
                    Log::error('O usuário ' . SessionHelper::getIdUsuario() . ' tentou acessar o recurso ' . $entity->id);
                    throw new EpgeException('Esse recurso não pertence a essa divisão');
                }
            }

            return $entity;
        } catch (Exception $exception) {
            Log::error($exception->getMessage() . $exception->getTraceAsString());
            throw $exception;
        }
    }

    /**
     *
     * @param string $columnWhere
     * @param array $columns
     * @return mixed
     */
    public function allByDivisao($columnWhere = 'divisao_id', $columns = ['*'], $param = null)
    {
        return $this->repository->allByDivisao($columnWhere, $columns = [
            '*'
        ], $param);
    }

    /**
     *
     * @param array $criteria
     * @param null $param
     * @return null
     * @throws Exception
     */
    public function findBy(array $criteria, $param = null)
    {
        try {
            return $this->repository->findBy($criteria, $param);
        } catch (Exception $e) {
            throw $e;
        }
        return null;
    }

    public function findBySql(array $criteria, $param = null)
    {
        try {
            return $this->repository->findBySql($criteria, $param);
        } catch (Exception $e) {
            throw $e;
        }
        return null;
    }

    public function findByInstituicao(array $criteria, $param = null)
    {
        $divisao['divisao_id'] = SessionHelper::getIdDivisao();
        $criteria = array_merge($criteria, $divisao);
        try {
            return $this->repository->findBy($criteria, $param);
        } catch (Exception $e) {
            throw $e;
        }
        return null;
    }

    /**
     *
     * @param int $pagination
     * @param null $criteria
     * @param null $param
     * @return null
     * @throws Exception
     */
    public function paginate($pagination = 30, $criteria = null, $param = null)
    {
        try {
            return $this->repository->paginate($pagination, $criteria, $param);
        } catch (Exception $e) {
            throw $e;
        }
        return null;
    }

    /**
     *
     * @param null $relation
     * @throws Exception
     */
    public function loadEntity($relation = null)
    {
        return $this->repository->loadEntity($relation);
    }

    /**
     * Retorna a instancia do service em um singleton
     * registrado no container do laravel
     *
     * (Overriding Container::make)
     *
     * @param string $abstract
     * @param array $parameters
     * @return mixed
     */
    public function make($abstract, array $parameters = [])
    {
        return parent::make($abstract, $parameters);
    }
}
