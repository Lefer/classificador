<?php
namespace App\Http\Services\Judicial;

use App\Http\Services\Base\BaseService;
use App\Http\Repositories\Judicial\ProcessoRepository;
use App\Http\Helpers\SessionHelper;

/**
 *
 * @author Leandro
 *
 */
class ProcessoService extends BaseService
{

    protected $repository;

    public function __construct(ProcessoRepository $repository)
    {
        $this->repository = $repository;
    }

    public function listarProcessos($dados)
    {
        $divisao = SessionHelper::getDivisao();

        return $this->repository->findBy([
            'divisao_id',
            '=>',
            $divisao->id
        ])->get();
    }
}