<?php
namespace App\Http\Requests\Base;

use Illuminate\Foundation\Http\FormRequest;

class GrupoRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'slug' => 'required',
            'name' => 'required',
            'divisao_id' => 'required'
        ];
        return $rules;
    }

    public function messages()
    {
        return [
            'slug.required' => 'O Alias deve ser informado.',
            'name.required' => 'A descrição deve ser informada.',
            'divisao_id.required' => 'A divisão deve ser informada.'
        ];
    }
}
