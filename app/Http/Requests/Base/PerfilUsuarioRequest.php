<?php
namespace App\Http\Requests\Base;

use Illuminate\Foundation\Http\FormRequest;

class PerfilUsuarioRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $id = $this->get('id');
        $password = $this->get('password');

        $rules = [
            'email' => 'required',
            'birthday' => 'required'
        ];

        if (empty($id) || ! empty($password)) {
            $rules = array_merge($rules, [
                'password' => 'required|min:6',
                'password_confirmation' => "required|min:8|same:password"
            ]);
        }
        return $rules;
    }

    public function attributes()
    {
        return [
            'email' => 'Email',
            'birthday' => 'Data de Nascimento',
            'password' => 'Senha',
            'password_confirmation' => 'Confirmação de Senha'
        ];
    }
}
