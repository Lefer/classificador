<?php
namespace App\Http\Requests\Base;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'login' => 'required',
            'password' => 'required'
        ];
        return $rules;
    }

    public function attributes()
    {
        return [
            'password' => 'Senha',
            'login' => 'Login'
        ];
    }
}
