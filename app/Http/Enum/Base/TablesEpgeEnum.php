<?php
namespace App\Http\Enum\Base;

/**
 * Class TablesEpgeEnum
 *
 * @package App\Http\Enum\Base
 */
class TablesEpgeEnum
{

    const BSE_SENTINEL_ACTIVATIONS = 'bse_sentinel_ativacao';

    const BSE_SENTINEL_PERSISTENCES = 'bse_sentinel_pesistencia';

    const BSE_SENTINEL_REMINDERS = 'bse_sentinel_lembrete';

    const BSE_SENTINEL_ROLES = 'bse_sentinel_grupo';

    const BSE_SENTINEL_ROLE_USERS = 'bse_sentinel_grupo_usuario';

    const BSE_SENTINEL_THROTTLE = 'bse_sentinel_acelerador';

    const BSE_SENTINEL_USERS = 'bse_sentinel_usuario';

    const BSE_DIVISAO_USUARIO = 'bse_divisao_usuario';

    const BSE_DIVISAO = 'bse_divisao';

    const BSE_TIPO_DIVISAO = 'bse_tipo_divisao';

    const BSE_PERMISSAO = 'bse_permissao';

    const BSE_PERMISSAO_TIPO_DIVISAO = 'bse_permissao_tipo_divisao';

    const BSE_CIDADE = 'bse_cidade';

    const BSE_ESTADO = 'bse_estado';

    const BSE_MENU = 'bse_menu';

    const BSE_PERMISSAO_PAGINA = 'bse_permisao_pagina';

    const BSE_CONFIGURACAO_SISTEMA = 'bse_configuracao_sistema';

    // ########## Tabelas do módulo administrativo #####################################################
    const ADM_PROCURADOR = 'adm_procurador';

    const ADM_PERMISSAO_PAGINAS = 'permissao_pagina';

    const ADM_LOG = 'adm_log';

    const ADM_PROCESSO = 'adm_processo';

    // ########## Tabelas do módulo Judicial #####################################################
    const JUD_PROCESSO_JUDICIAL = 'jud_processo_judicial';

    const JUD_CLASSE_JUDICIAL = 'jud_classe_judicial';

    const JUD_ASSUNTO_JUDICIAL = 'jud_assunto_judicial';

    const JUD_LOCALIDADE_JUDICIAL = 'jud_localidade_judicial';

    const JUD_STATUS_PROCESSO_JUDICIAL = 'jud_status_processo_judicial';

    const JUD_MAGISTRADO = 'jud_magistrado';

    const JUD_EVENTO_JUDICIAL = 'jud_evento_judicial';

    const JUD_MOVIMENTO_JUDICIAL = 'jud_movimento_judicial';

    const JUD_PARTE_PROCESSO_JUDICIAL = 'jud_parte_processo_judicial';

    const JUD_REPRESENTANTE_PARTE_JUDICIAL = 'jud_representante_parte_judicial';

    const JUD_PRIORIDADE_PROCESSO_JUDICIAL = 'jud_prioridade_processo_judicial';

    const JUD_PROCESSO_JUDICIAL_RELACIONADO = 'jud_processo_judicial_relacionado';

    const JUD_ORGAO_JULGADOR = 'jud_orgao_julgador';

    const JUD_COMPETENCIA_JUDICIAL = 'jud_competencia_judicial';

    const JUD_DISTRIBUICAO = 'jud_distribuicao';
}
