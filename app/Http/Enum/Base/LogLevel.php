<?php
namespace App\Http\Enum\Base;

abstract class LogLevel extends Enum
{

    const ERROR = 0;

    const SUCCESS = 1;

    const ALERT = 2;

    const INFO = 3;

    const MODO = [
        self::ERROR => 'Erro',
        self::SUCCESS => 'Sucesso',
        self::ALERT => 'Alerta',
        self::INFO => 'Info'
    ];

    const CSS = [
        self::ERROR => 'tag-error',
        self::SUCCESS => 'tag-success',
        self::ALERT => 'tag-warning',
        self::INFO => 'tag-info'
    ];
}
