<?php
namespace App\Http\Enum\Base;

use App\Http\Helpers\SessionHelper;

/**
 * Class TipoInstituicao
 *
 * @package App\Http\Enum
 */
abstract class TipoDivisaoEnum extends Enum
{

    const GERAL = 1;

    const RESTRITO = 2;

    const MODO = [
        self::GERAL => 'Acesso Geral',
        self::RESTRITO => 'Acesso Restrito'
    ];

    /**
     * Esse método liberar visualização de tudo
     *
     * @param
     *            $tipoInstituicao
     * @return bool
     */
    public static function isPermissaoGlobal($tipoDivisao = null): bool
    {
        if (isset($tipoDivisao)) {
            return ((int) $tipoDivisao->permissao == TipoDivisaoEnum::GERAL);
        }
        return ((int) SessionHelper::getInstituicao()->tipoDivisao->permissao == TipoDivisaoEnum::GERAL);
    }

    /**
     * Esse método liberar visualização de tudo
     *
     * @param
     *            $tipoInstituicao
     * @return bool
     */
    public static function isPermissaoRestrita($divisao = null): bool
    {
        if (isset($divisao)) {
            return ((int) $divisao->tipoDivisao->permissao == TipoDivisaoEnum::RESTRITO);
        }
        return ((int) SessionHelper::getDivisao()->tipoDivisao->permissao == TipoDivisaoEnum::RESTRITO);
    }
}
