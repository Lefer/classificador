<?php
namespace App\Http\Enum\Base;

abstract class ModuloEnum extends Enum
{

    const ADMINISTRATIVO = 'ADMINISTRATIVO';

    const JUDICIAL = 'JUDICIAL';

    const MODULOS = [
        self::ADMINISTRATIVO => [
            'id' => 1,
            'label' => 'Administrativo',
            'namespace' => 'Admin',
            'modulo' => 'ADMINISTRATIVO',
            'skin' => '#696969',
            'vision' => 'fechado'
        ],
        self::JUDICIAL => [
            'id' => 2,
            'label' => 'Judicial',
            'namespace' => 'Judicial',
            'modulo' => 'JUDICIAL',
            'skin' => '#3F51B5',
            'vision' => 'aberto'
        ]
    ];

    const SERVICOS = [
        self::ADMINISTRATIVO => self::SERVICOS_ADMINISTRATIVOS,
        self::JUDICIAL => self::SERVICOS_JUDICIAIS
    ];

    const SERVICOS_ADMINISTRATIVOS = [];

    const SERVICOS_JUDICIAIS = [
        self::DISTRIBUICAO_DE_PROCESSOS => "Distribuição de Processo"
    ];
}
