<?php
use Illuminate\Database\Seeder;

class ProcessosSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("INSERT INTO processo (id_processo, seq_processo, num_processo, id_assunto_principal) VALUES (711568737492369720340000000001, 1, '00108441620188272729', 10481); ");
        DB::statement("INSERT INTO processo (id_processo, seq_processo, num_processo, id_assunto_principal) VALUES (711546632049519730330000000001, 1, '00003869420188272710', 9582); ");
        DB::statement("INSERT INTO processo (id_processo, seq_processo, num_processo, id_assunto_principal) VALUES (711547032798903320350000000001, 1, '00034489020158272729', 7704); ");
        DB::statement("INSERT INTO processo (id_processo, seq_processo, num_processo, id_assunto_principal) VALUES (711546544471392190330000000001, 1, '00068366420168272729', 9607); ");
        DB::statement("INSERT INTO processo (id_processo, seq_processo, num_processo, id_assunto_principal) VALUES (711547731954147310370000000001, 1, '00012090520178272710', 100035); ");
        DB::statement("INSERT INTO processo (id_processo, seq_processo, num_processo, id_assunto_principal) VALUES (711546538416971620340000000001, 1, '00011940720158272710', 10437); ");
        DB::statement("INSERT INTO processo (id_processo, seq_processo, num_processo, id_assunto_principal) VALUES (711546629236974790340000000001, 1, '00064457520178272729', 9587); ");
        DB::statement("INSERT INTO processo (id_processo, seq_processo, num_processo, id_assunto_principal) VALUES (711546450528530590330000000002, 1, '00078459520158272729', 4960); ");
        DB::statement("INSERT INTO processo (id_processo, seq_processo, num_processo, id_assunto_principal) VALUES (711546820322164020380000000001, 1, '00251364520148272729', 7780); ");
        DB::statement("INSERT INTO processo (id_processo, seq_processo, num_processo, id_assunto_principal) VALUES (711546783964130190350000000001, 1, '00394511020168272729', 10441); ");
        DB::statement("INSERT INTO processo (id_processo, seq_processo, num_processo, id_assunto_principal) VALUES (711546448672383250370000000001, 1, '00014585820148272710', 100044); ");
        DB::statement("INSERT INTO processo (id_processo, seq_processo, num_processo, id_assunto_principal) VALUES (711546625340136470330000000001, 1, '00000231520158272710', 4870); ");
        DB::statement("INSERT INTO processo (id_processo, seq_processo, num_processo, id_assunto_principal) VALUES (711546451693851830350000000001, 1, '00045916920188272710', 6226); ");
        DB::statement("INSERT INTO processo (id_processo, seq_processo, num_processo, id_assunto_principal) VALUES (711546543949100780350000000001, 1, '00024325620188272710', 7698); ");
        DB::statement("INSERT INTO processo (id_processo, seq_processo, num_processo, id_assunto_principal) VALUES (711546446118286060340000000001, 1, '00060046520158272729', 10437); ");
        DB::statement("INSERT INTO processo (id_processo, seq_processo, num_processo, id_assunto_principal) VALUES (711547051129459880370000000005, 1, '00178943520148272729', 10582); ");
        DB::statement("INSERT INTO processo (id_processo, seq_processo, num_processo, id_assunto_principal) VALUES (711546457284144840380000000001, 1, '00049985720148272729', 100044); ");
        DB::statement("INSERT INTO processo (id_processo, seq_processo, num_processo, id_assunto_principal) VALUES (711557776718105250330000000001, 1, '50014815020138272710', 10481); ");
        DB::statement("INSERT INTO processo (id_processo, seq_processo, num_processo, id_assunto_principal) VALUES (711546469585393050340000000001, 1, '00020711020168272710', 9587); ");
        DB::statement("INSERT INTO processo (id_processo, seq_processo, num_processo, id_assunto_principal) VALUES (711546880997309080330000000001, 1, '00050115620148272729', 7780); ");
        DB::statement("INSERT INTO processo (id_processo, seq_processo, num_processo, id_assunto_principal) VALUES (711546540936462260370000000001, 1, '00070955920168272729', 6226); ");
        DB::statement("INSERT INTO processo (id_processo, seq_processo, num_processo, id_assunto_principal) VALUES (711546448649235180370000000001, 1, '00022033320178272710', 4960); ");
        DB::statement("INSERT INTO processo (id_processo, seq_processo, num_processo, id_assunto_principal) VALUES (711546521115214950390003000001, 1, '00029132420158272710', 9607); ");
        DB::statement("INSERT INTO processo (id_processo, seq_processo, num_processo, id_assunto_principal) VALUES (711548186788695850340000000001, 1, '00068151420178272710', 100035); ");
        DB::statement("INSERT INTO processo (id_processo, seq_processo, num_processo, id_assunto_principal) VALUES (711546621229231640350000000001, 1, '00046791020188272710', 7698); ");
        DB::statement("INSERT INTO processo (id_processo, seq_processo, num_processo, id_assunto_principal) VALUES (711546610305510230370000000001, 1, '00046566420188272710', 9582); ");
        DB::statement("INSERT INTO processo (id_processo, seq_processo, num_processo, id_assunto_principal) VALUES (711546945815438870340000000001, 1, '00007427120148272729', 4970); ");
        DB::statement("INSERT INTO processo (id_processo, seq_processo, num_processo, id_assunto_principal) VALUES (711546825159869310370000000001, 1, '00028460220158272729', 10441); ");
        DB::statement("INSERT INTO processo (id_processo, seq_processo, num_processo, id_assunto_principal) VALUES (711547208403557280370000000001, 1, '00007045920148272729', 7704); ");
        DB::statement("INSERT INTO processo (id_processo, seq_processo, num_processo, id_assunto_principal) VALUES (711546698288128050330000000001, 1, '00017351720148272729', 10582); ");
    }
}
