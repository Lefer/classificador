<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcessoTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('processo', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->decimal('id_processo', 30, 0);
            $table->integer('seq_processo');
            $table->string('num_processo', 20);
            $table->integer('id_assunto_principal');
            $table->integer('id_assunto_principal_classificado')->nullable();
            $table->dateTime('data_classificacao')->nullable();
            $table->text('classificador')->nullable();
            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('processo');
    }
}
