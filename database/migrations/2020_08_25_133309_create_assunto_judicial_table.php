<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssuntoJudicialTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assunto_judicial', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_assunto_judicial')->unsigned();
            $table->string('cod_assunto', 20);
            $table->string('des_assunto', 150);
            $table->integer('cod_mni_assunto_judicial')->nullable();
            $table->char('tipo_assunto', 1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assunto_judicial');
    }
}
