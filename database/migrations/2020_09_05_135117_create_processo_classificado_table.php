<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcessoClassificadoTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('processo_classificado', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('processo_id');
            $table->string('numemro_processo', 30);
            $table->bigInteger('assunto_id');
            $table->string('classificador', 200);

            $table->timestamps();

            $table->index('processo_id');
            $table->index('numemro_processo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('processo_classificado');
    }
}
